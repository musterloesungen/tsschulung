
interface Rechnung {
  id: number;
  kunde: string;
  datum: string;
  betrag: number;
  beschreibung: string;
}

function berechneSumme(rechnungen: Rechnung[]): number {
  return rechnungen.reduce((summe, rechnung) => summe + rechnung.betrag, 0);
}

const rechnungsdaten: Rechnung[] = [
  { id: 1, kunde: "Kunde A", datum: "2024-01-01", betrag: 100.0, beschreibung: "Dienstleistung A" },
  { id: 2, kunde: "Kunde B", datum: "2024-01-05", betrag: 250.5, beschreibung: "Dienstleistung B" },
  { id: 3, kunde: "Kunde C", datum: "2024-01-10", betrag: 75.25, beschreibung: "Dienstleistung C" },
  { id: 4, kunde: "Kunde A", datum: "2024-01-15", betrag: 300.75, beschreibung: "Dienstleistung D" },
  { id: 5, kunde: "Kunde B", datum: "2024-01-20", betrag: 150.0, beschreibung: "Dienstleistung E" },
  { id: 6, kunde: "Kunde B", datum: "2024-01-25", betrag: 220.45, beschreibung: "Dienstleistung F" },
  { id: 7, kunde: "Kunde A", datum: "2024-01-30", betrag: 310.0, beschreibung: "Dienstleistung G" },
  { id: 8, kunde: "Kunde C", datum: "2024-02-05", betrag: 450.0, beschreibung: "Dienstleistung H" },
  { id: 9, kunde: "Kunde C", datum: "2024-02-10", betrag: 85.0, beschreibung: "Dienstleistung I" },
  { id: 10, kunde: "Kunde D", datum: "2024-02-15", betrag: 500.0, beschreibung: "Dienstleistung J" },
];

const gesamtbetrag = berechneSumme(rechnungsdaten);
console.log(`Die Summe aller Rechnungsbeträge beträgt: ${gesamtbetrag}`);
