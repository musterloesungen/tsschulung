# Aufgabe zur Einführung in die Objektorientierung in TypeScript

## Beschreibung:

Die Aufgabe besteht darin, die Funktionalität der `mergeObjects`-Funktion aus dem Beispielcode im Chat mithilfe einer Klasse zu implementieren.

### Erläuterung der Syntax:

In TypeScript können Klassen verwendet werden, um Objekte zu erstellen, die Methoden und Eigenschaften enthalten. Die wichtigsten Schlüsselwörter und Konzepte sind:

- **class**: Definiert eine neue Klasse.
- **constructor**: Eine spezielle Methode zum Initialisieren von Objekten.
- **public**: Mitglieder, die von überall aus zugänglich sind (Standard, wenn nichts angegeben ist).
- **private**: Mitglieder, die nur innerhalb der Klasse zugänglich sind.
- **protected**: Mitglieder, die in der Klasse und in abgeleiteten Klassen zugänglich sind.
- **methods**: Funktionen, die innerhalb der Klasse definiert sind.

### Aufgabe:

1. Definieren Sie eine Klasse `Merger`, die Objekte zusammenführen kann.
2. Die Klasse sollte eine Methode `addObject` haben, um Objekte hinzuzufügen.
3. Implementieren Sie eine Methode `getMergedObject`, die das zusammengeführte Objekt zurückgibt.
4. Verwenden Sie private und public Schlüsselwörter, um die Zugänglichkeit der Methoden und Eigenschaften zu steuern.

### Beispielcode:

```typescript
class Merger {
  // Private Eigenschaft, um die Liste der Objekte zu speichern
  private objects: { id: number; [key: string]: any }[] = [];

  // Methode zum Hinzufügen eines Objekts
  public addObject(obj: { id: number; [key: string]: any }): void {
    this.objects.push(obj);
  }

  // Methode zum Zusammenführen der Objekte
  public getMergedObject(): { id: number; [key: string]: any } {
    return this.objects.reduce((merged, current) => {
      return { ...merged, ...current };
    }, {});
  }
}

// Beispielverwendung der Merger-Klasse
const merger = new Merger();

const obj1 = { id: 1, name: 'Alice' };
const obj2 = { id: 1, age: 25 };
const obj3 = { id: 1, city: 'Wonderland' };

merger.addObject(obj1);
merger.addObject(obj2);
merger.addObject(obj3);

const mergedObj = merger.getMergedObject();

console.log(mergedObj); // { id: 1, name: 'Alice', age: 25, city: 'Wonderland' }
```

### Erklärung:

1. **Klasse definieren**: Die Klasse `Merger` wird mit dem Schlüsselwort `class` definiert.
2. **Konstruktor und Eigenschaften**: Der Konstruktor wird verwendet, um die Objekteigenschaft zu initialisieren. Die Eigenschaft `objects` ist privat und speichert die hinzuzufügenden Objekte.
3. **Methoden**:
    - `addObject`: Eine öffentliche Methode zum Hinzufügen eines Objekts zur Liste.
    - `getMergedObject`: Eine öffentliche Methode, die die Objekte zusammenführt und das resultierende Objekt zurückgibt.
4. **Verwendung**: Eine Instanz der Klasse `Merger` wird erstellt, und die Objekte werden mithilfe der Methode `addObject` hinzugefügt. Schließlich wird das zusammengeführte Objekt mit `getMergedObject` abgerufen und ausgegeben.

Diese Aufgabe soll die Teilnehmer in die objektorientierte Syntax von TypeScript einführen und gleichzeitig das Verständnis der zuvor gelernten Konzepte vertiefen.
